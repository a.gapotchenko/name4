<html lang="ru">
<head>
    <title>Задание 4</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="style.css" type="text/css" rel="stylesheet">
</head>
<body>
<!DOCTYPE html>
<html>
<head>
    <title>form</title>
    <link rel="stylesheet" href="style.css" type="text/css">
</head>
<body>
<div class="main">
    <div class="content">
        <form id="form" action="" method="POST">
            <?php
                if (!empty($messages)) {
                print('<div id="messages">');
                // Выводим все сообщения.
                foreach ($messages as $message) {
                    print($message);
                }
                print('</div>');
            }
            ?>
            <div class="form">
                <div class="form_title">
                    <h1>Обратная связь</h1>
                </div>
                <p  for="nameInput"> Имя   </p>
                <p><input id="nameInput" type="text" name="name"  <?php if ($errors['name']) {print 'class="error"';} ?> value="<?php print $values['name']; ?>" /></p>
                <p for="emailInput"> Почта  </p>
                <p><input id="emailInput" type="email" name="email" <?php if ($errors['email']) {print 'class="error"';} ?> value="<?php print $values['email']; ?>" />
                </p>
                <p for="selectInput"> Год рождения </p>
                <select name="year">
                    <?php
                    for ($i = 2021; $i > 1991; $i--) {
                        print('<option value="'.$i.'" ');
                        if ($values['year'] == $i) print('selected ');
                        print('>'.$i.'</option> ');
                    }
                    ?>
                </select>
                <lable for="sexInput"> Пол </lable>
                <p></p>
                <label>М
                    <input type="radio" name="sex" id="sexInput" value="he" <?php if ($values['sex'] == 'he') print("checked"); ?> >
                    <span></span>
                </label>
                <label>Ж
                    <input type="radio" name="sex" id="sexInput" value="she" <?php if ($values['sex'] == 'she') print("checked"); ?> >
                    <span></span>
                </label>
                <label>Оно
                    <input type="radio" name="sex" id="sexInput" value="it" <?php if ($values['sex'] == 'it') print("checked"); ?> >
                    <span></span>
                </label>
                <p> Количество конечностей </p>
                <label>1
                    <input type="radio" name="limbs" value="1" <?php if ($values['limbs'] == 1) print("checked"); ?> >
                    <span></span>
                </label>
                <label>2
                    <input type="radio" name="limbs" value="2" <?php if ($values['limbs'] == 2) print("checked"); ?> >
                    <span></span>
                </label>
                <label>3
                    <input type="radio" name="limbs" value="3" <?php if ($values['limbs'] == 3) print("checked"); ?> >
                    <span></span>
                </label>
                <label>Болеше, чем надо
                    <input type="radio" name="limbs" value="4" <?php if ($values['limbs'] == 4) print("checked"); ?> >
                    <span></span>
                </label>
                <p for="powersSelect">Суперспособности</p>
                <select id="powersSelect" <?php if ($errors['powers']) {print 'class="error"';} ?> name="powers[]" multiple size="4">
                    <?php
                    foreach ($powers as $key => $value) {
                         $selected = empty($values['powers'][$key]) ? '' : ' selected="selected" ';
                         printf('<option value="%s",%s>%s</option>', $key, $selected, $value);
                    }
                    ?>
                </select>
                <p>Биография.</p>
                <textarea id="bioArea" name="bio" <?php if ($errors['bio']) {print 'class="error"';} ?>><?php print $values['bio']; ?></textarea>
                <label <?php if ($errors['check']) {print 'class="error"';} ?>><input type="checkbox" name="check" value="ok"> С контрактом ознакомлен(а)</label>
                <input type="submit" value="Отправить" />
        </form>
    </div>
</div>
</div>
</body>
</html>
